<h1 align="center">Teste Lyncas Java - JSF</h1>

[![Lyncas Logo](https://img-dev.feedback.house/TCo5z9DrSyX0EQoakV8sJkx1mSg=/fit-in/300x300/smart/https://s3.amazonaws.com/feedbackhouse-media-development/modules%2Fcore%2Fcompany%2F5c9e1b01c5f3d0003c5fa53b%2Flogo%2F5c9ec4f869d1cb003cb7996d)](https://www.lyncas.net)

<h4 align="center">
  Sistema desenvolvido utilizando a tecnologia Java e Java Server Faces (JSF).
</h4>

<p align="center">
  <a href="#tecnologias">Tecnologias</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#Como-Usar">Como Usar</a>
</p>

## Tecnologias

Este projeto foi desenvolvido como para o processo seletivo para a empresa [Lyncas](https://www.linkedin.com/company/lyncas-net/) com as seguintes tecnologias:

- [Java](https://www.java.com/pt_BR/download/faq/develop.xml)
- [Java Server Faces](http://www.javaserverfaces.org/)
- [Primefaces](https://www.primefaces.org/)
- [MySQL](https://www.mysql.com/)
- [Hibernate](https://hibernate.org/)
- [Eclipse](https://www.eclipse.org/)
- [Apache Tomcat 9](https://tomcat.apache.org/download-90.cgi)
- [JUnit](https://junit.org/)

## Como Usar

Para clonar e executar este aplicativo, você precisará [Git](https://git-scm.com) instalado no seu computador. Executar no seu terminal ou prompt de comando:

```bash
# Para clonar este repositório
$ git clone https://gitlab.com/osterloh/nodejs.git

# Entrar no repositório
$ cd nodejs/Teste-Lyncas

# Instalar as dependências
$ mvn install

```

Após instalar as dependências, pode-se importar o projeto para sua IDE de desenvolvimento Java, durante o processo de progaramação deste projeto foi utilizado a IDE Eclipse.

Será necessário configurar a Base de Dados, informando o database que será utilizado, usuário e senha no arquivo `persistence.xml` no diretório `Java Resources/src/main/java/META-INF` e passar os dados do data base utilizado, assim como criar a base de dados no SGBD, para este projeto foi utilizado o MySQL.

Para compilar os teste será necessário descomentar os testes, da calase de `TestPessoa.java`, `Java Resources/src/test/java/br.com.lyncas`, que deseja executar.
Para compilar o projeto, durante o desenvolvimento foi utilizado o servidor Apache Tomcat 9.

---

Tempo de desenvolvimento: 20hrs

Desenvolvido por [Johnatan Luiz Osterloh](https://www.linkedin.com/in/johnatanosterloh/)
