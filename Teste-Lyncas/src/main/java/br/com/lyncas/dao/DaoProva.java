package br.com.lyncas.dao;

import br.com.lyncas.model.Prova;

public class DaoProva<E> extends DaoGeneric<Prova> {
	
	public void removeProva(Prova prova) throws Exception {
		getEntityManager().getTransaction().begin();
		
		String sqlDeleteProva = "delete from questao where prova_id = " + prova.getId();
		getEntityManager().createNativeQuery(sqlDeleteProva).executeUpdate();
		getEntityManager().getTransaction().commit();
		
		super.delete(prova);
	}

}
