package br.com.lyncas.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.lyncas.HibernateUtil;

public class DaoGeneric<T> {

	private EntityManager entityManager = HibernateUtil.getEntityManager();

	public void insert(T entidade) {
		EntityTransaction transaction = entityManager.getTransaction();
		transaction.begin();
		entityManager.persist(entidade);
		transaction.commit();
	}

	public List<T> selectAll(Class<T> entidade) {
		EntityTransaction transaction = entityManager.getTransaction();
		transaction.begin();

		List<T> lista = entityManager.createQuery(" from " + entidade.getName()).getResultList();

		transaction.commit();

		return lista;
	}

	public T search(T entidade) {
		Object id = HibernateUtil.getPrimaryKey(entidade);
		T item = (T) entityManager.find(entidade.getClass(), id);

		return item;
	}
	
	public T search(long id, Class<T> entidade) {
		entityManager.clear();
		T item = (T) entityManager.createQuery("from " + entidade.getSimpleName() + " where id = " + id).getSingleResult();

		return item;
	}

	public T update(T entidade) {
		EntityTransaction transaction = entityManager.getTransaction();
		transaction.begin();
		T editItem = (T) entityManager.merge(entidade);
		transaction.commit();

		return editItem;
	}

	public void delete(T entidade) throws Exception {
		Object id = HibernateUtil.getPrimaryKey(entidade);

		EntityTransaction transaction = entityManager.getTransaction();
		transaction.begin();

		entityManager
				.createNativeQuery(
						"delete from " + entidade.getClass().getSimpleName().toLowerCase() + " where id = " + id)
				.executeUpdate();
		
		transaction.commit();
	}
	
	public EntityManager getEntityManager() {
		return entityManager;
	}

}
