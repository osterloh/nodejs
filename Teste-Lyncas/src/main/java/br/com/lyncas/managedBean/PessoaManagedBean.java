package br.com.lyncas.managedBean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.com.lyncas.dao.DaoGeneric;
import br.com.lyncas.model.Pessoa;

@ManagedBean(name = "pessoaManagedBean")
@ViewScoped
public class PessoaManagedBean {
	
	private Pessoa pessoa = new Pessoa();
	private DaoGeneric<Pessoa> daoGeneric = new DaoGeneric<Pessoa>();
	private List<Pessoa> pessoas = new ArrayList<Pessoa>();

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public String insertPessoa() {
		try {
			pessoa.setNome(pessoa.getNome().toLowerCase());
			pessoa.setEmail(pessoa.getEmail().toLowerCase());

			if (pessoa.getNome() == null || pessoa.getEmail() == null || pessoa.getSenha() == null
					|| pessoa.getTipo() == null) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
						"Informação", "Os campos Nome, E-Mail, Tipo e Senha são de preenchimento obrigatório!!!"));

			} else if (pessoa.getNome() == "" || pessoa.getEmail() == "" || pessoa.getSenha() == ""
					|| pessoa.getTipo() == "") {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
						"Informação", "Os campos Nome, E-Mail, Tipo e Senha são de preenchimento obrigatório!!!"));

			} else {
				daoGeneric.insert(pessoa);
				pessoas.add(pessoa);

				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_INFO, "Informação", "Pessoa cadastrada com sucesso!!!"));

				newPessoa();

			}

		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Informação", "Erro ao cadastrar, E-Mail ou Nome já cadastrado!!!"));
			newPessoa();
		}

		return "";
	}

	public String newPessoa() {
		pessoa = new Pessoa();

		return "";
	}

	public List<Pessoa> getListPessoas() {
		pessoas = daoGeneric.selectAll(Pessoa.class);

		return pessoas;
	}

	public String deletePessoa() {
		try {
			daoGeneric.delete(pessoa);
			pessoas.remove(pessoa);
			pessoa = new Pessoa();
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Informação", "Dados removidos com sucesso!!!"));

		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Informação", "Erro ao remover os dados!!!"));
		}

		return "";
	}

}
