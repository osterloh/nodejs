package br.com.lyncas.managedBean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.com.lyncas.dao.DaoQuestaoProva;
import br.com.lyncas.dao.DaoResposta;
import br.com.lyncas.model.Questao;
import br.com.lyncas.model.RespostaQuestao;

@ManagedBean(name = "respostaManagedBean")
@ViewScoped
public class RespostaManagedBean {

	private RespostaQuestao respostaQuestao = new RespostaQuestao();
	private Questao questao = new Questao();
	private DaoQuestaoProva<Questao> daoQuestaoProva = new DaoQuestaoProva<Questao>();
	private DaoResposta<RespostaQuestao> daoGeneric = new DaoResposta<RespostaQuestao>();
	private List<RespostaQuestao> respostas = new ArrayList<RespostaQuestao>();

	@PostConstruct
	public void init() {
		String idQuestao = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("questaoId");
		questao = daoQuestaoProva.search(Long.parseLong(idQuestao), Questao.class);

	}

	public Questao getQuestao() {
		return questao;
	}

	public void setQuestao(Questao questao) {
		this.questao = questao;
	}

	public RespostaQuestao getRespostaQuestao() {
		return respostaQuestao;
	}

	public void setRespostaQuestao(RespostaQuestao respostaQuestao) {
		this.respostaQuestao = respostaQuestao;
	}

	public String insertResposta() {
		try {
			if (respostaQuestao.getDescricaoResposta() == null || respostaQuestao.getDescricaoResposta() == "") {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
						"Informação", "O campo Descrição de resposta é de preenchimento obrigatório!!!"));
			} else {
				getListRespostas();
				System.out.println("Qtd: " + respostas.size());

				if (respostas.size() >= 4) {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
							"Informação", "Já atingiu o lite de 4(quatro) respostas por questão!!!"));

				} else {

					respostaQuestao.setQuestao(questao);
					daoGeneric.insert(respostaQuestao);
					questao = daoQuestaoProva.search(questao.getId(), Questao.class);

					newResposta();

					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
							"Informação", "Resposta cadastrada com sucesso!!!"));
				}

			}

		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Informação", "Erro ao cadastrar a resposta para a questão da prova, Resposta já cadastrado!!!"));

			newResposta();
		}

		return "";
	}

	public String newResposta() {
		respostaQuestao = new RespostaQuestao();

		return "";
	}

	public List<RespostaQuestao> getListRespostas() {
		respostas = daoGeneric.selectAll(RespostaQuestao.class);

		return respostas;
	}

	public String deleteResposta() {
		try {
			daoGeneric.delete(respostaQuestao);
			respostas.remove(respostaQuestao);
			questao = daoQuestaoProva.search(questao.getId(), Questao.class);
			respostaQuestao = new RespostaQuestao();
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Informação", "Dados removidos com sucesso!!!"));
			
			newResposta();

		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Informação", "Erro ao remover os dados!!!"));
		}

		return "";
	}

}
