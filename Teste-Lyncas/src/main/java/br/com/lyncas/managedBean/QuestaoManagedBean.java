package br.com.lyncas.managedBean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.com.lyncas.dao.DaoProva;
import br.com.lyncas.dao.DaoQuestaoProva;
import br.com.lyncas.model.Prova;
import br.com.lyncas.model.Questao;

@ManagedBean(name = "questoaManagedBean")
@ViewScoped
public class QuestaoManagedBean {

	private Questao questao = new Questao();
	private Prova prova = new Prova();
	private DaoProva<Prova> daoProva = new DaoProva<Prova>();
	private DaoQuestaoProva<Questao> daoGeneric = new DaoQuestaoProva<Questao>();
	private List<Questao> questoes = new ArrayList<Questao>();

	@PostConstruct
	public void init() {
		String idProva = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("provaId");
		System.out.println("Prova: " + idProva);

		prova = daoProva.search(Long.parseLong(idProva), Prova.class);
		System.out.println("Prova: " + prova.getId());
	}

	public Prova getProva() {
		return prova;
	}

	public void setProva(Prova prova) {
		this.prova = prova;
	}

	public Questao getQuestao() {
		return questao;
	}

	public void setQuestao(Questao questao) {
		this.questao = questao;
	}

	public String insertQuestaoProva() {
		try {

			if (questao.getTitulo() == null || questao.getTitulo() == "") {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
						"Informação", "O campo Título é de preenchimento obrigatório!!!"));

			} else {
				getListQuestoes();
				for(int i = 0; i <= questoes.size(); i++) {
					if(questoes.get(i).equals(questao)) {
						newQuestaoProva();
						
					} else {
						questao.setProva(prova);
						daoGeneric.insert(questao);
						prova = daoProva.search(prova.getId(), Prova.class);

						FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
								"Informação", "Questão cadastrada com sucesso!!!"));
						
						break;
						
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Informação", "Erro ao cadastrar a questão para a prova, Título já cadastrado!!!"));

			newQuestaoProva();
		}

		return "";
	}

	public String newQuestaoProva() {
		questao = new Questao();

		return "";
	}

	public List<Questao> getListQuestoes() {
		questoes = daoGeneric.selectAll(Questao.class);

		return questoes;
	}

	public String deleteQuestao() {
		try {
			daoGeneric.delete(questao);
			questoes.remove(questao);
			prova = daoProva.search(prova.getId(), Prova.class);
			questao = new Questao();
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Informação", "Dados removidos com sucesso!!!"));
			
			newQuestaoProva();

		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Informação", "Erro ao remover os dados!!!"));
		}

		return "";
	}

}
