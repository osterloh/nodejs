package br.com.lyncas.managedBean;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.com.lyncas.model.Pessoa;

@ManagedBean(name = "loginManagedBean")
@ViewScoped
public class LoginManagedBean extends Pessoa {

	private PessoaManagedBean pessoaManagedBean = new PessoaManagedBean();

	private boolean logado = false;
	private String user;
	private String passwd;

	public Object logar() {

		for (int i = 0; i < pessoaManagedBean.getListPessoas().size(); i++) {
			if (pessoaManagedBean.getListPessoas().get(i).getEmail().equals(getEmail())
					&& pessoaManagedBean.getListPessoas().get(i).getSenha().equals(getSenha())) {

				setUser(getEmail());
				setPasswd(getSenha());
				break;
			}
		}

		try {
			if (getUser().equals(getEmail()) && getPasswd().equals(getSenha())) {
				setLogado(true);

				return "/cadPessoas";
			} else {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_WARN, "Informação", "Usuário ou Senha incorrento!!!"));
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, "Informação", "Erro ao validar, tente novamente!!!"));

		}

		return "";
	}
	
	public String logof() {
		setLogado(false);
		
		return "/login";
	}
	
	public String getUser() {
		return user;
	}
	
	public void setUser(String user) {
		this.user = user;
	}
	
	public String getPasswd() {
		return passwd;
	}
	
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public boolean getLogado() {
		return logado;
	}

	public void setLogado(boolean logado) {
		this.logado = logado;
	}

}
