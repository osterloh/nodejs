package br.com.lyncas.managedBean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.com.lyncas.dao.DaoProva;
import br.com.lyncas.model.Prova;

@ManagedBean(name = "provaManegedBean")
@ViewScoped
public class ProvaManagedBean {

	private Prova prova = new Prova();
	private List<Prova> provas = new ArrayList<Prova>();
	private DaoProva<Prova> daoGeneric = new DaoProva<Prova>();

	@PostConstruct
	public void init() {
		provas = daoGeneric.selectAll(Prova.class);
	}

	public Prova getProva() {
		return prova;
	}

	public void setProva(Prova prova) {
		this.prova = prova;
	}

	public String insertProva() {
		try {

			if (prova.getDescricao() == null || prova.getDescricao() == "") {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
						"Informação", "O campo Descrição é de preenchimento obrigatório!!!"));

			} else {
				daoGeneric.insert(prova);
				provas.add(prova);

				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_INFO, "Informação", "Prova cadastrada com sucesso!!!"));

				newProva();
			}

		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Informação", "Erro ao cadastrar a prova, descrição já cadastrada!!!"));
			newProva();

		}

		return "";
	}

	public String newProva() {
		prova = new Prova();

		return "";
	}

	public List<Prova> getListProva() {
		return provas;
	}

	public String deleteProva() {
		try {
			daoGeneric.removeProva(prova);
			provas.remove(prova);
			prova = new Prova();
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Informação", "Dados removidos com sucesso!!!"));

		} catch (Exception e) {
			System.out.println("ERRO NO DELETE PROVA");
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Informação", "Erro ao remover os dados!!!"));
		}
		return "";
	}

}
