package br.com.lyncas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class RespostaQuestao {

	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false, unique = true)
	private String descricaoResposta;
	@Column(nullable = false)
	private boolean verdadeira;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Questao questao;

	public Questao getQuestao() {
		return questao;
	}

	public void setQuestao(Questao questao) {
		this.questao = questao;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescricaoResposta() {
		return descricaoResposta;
	}

	public void setDescricaoResposta(String descricaoResposta) {
		this.descricaoResposta = descricaoResposta;
	}

	public boolean getVerdadeira() {
		return verdadeira;
	}

	public void setVerdadeira(boolean verdadeira) {
		this.verdadeira = verdadeira;
	}

	@Override
	public String toString() {
		return "RespostaQuestao [id=" + id + ", descricaoResposta=" + descricaoResposta + ", verdadeira=" + verdadeira
				+ ", questao=" + questao + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RespostaQuestao other = (RespostaQuestao) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
