package br.com.lyncas.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Questao {

	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false, unique = true)
	private String titulo;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Prova prova;

	@OneToMany(mappedBy = "questao", fetch = FetchType.EAGER)
	private List<RespostaQuestao> respostasQuestao;

	public List<RespostaQuestao> getRespostasQuestao() {
		return respostasQuestao;
	}

	public void setRespostasQuestao(List<RespostaQuestao> respostasQuestao) {
		this.respostasQuestao = respostasQuestao;
	}

	public Prova getProva() {
		return prova;
	}

	public void setProva(Prova prova) {
		this.prova = prova;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	@Override
	public String toString() {
		return "Questao [id=" + id + ", titulo=" + titulo + ", prova=" + prova + ", respostasQuestao="
				+ respostasQuestao + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Questao other = (Questao) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
