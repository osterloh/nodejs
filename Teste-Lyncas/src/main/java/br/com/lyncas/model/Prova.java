package br.com.lyncas.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Prova {

	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false, unique = true)
	private String descricao;
	private boolean ativa;
	private long totalRealizadas;

	@OneToMany(mappedBy = "prova", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE, orphanRemoval = true)
	private List<Questao> questoes;

	public List<Questao> getQuestoes() {
		return questoes;
	}

	public void setQuestoes(List<Questao> questoes) {
		this.questoes = questoes;
	}

	public boolean getAtiva() {
		return ativa;
	}

	public void setAtiva(boolean ativa) {
		this.ativa = ativa;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public long getTotalRealizadas() {
		return totalRealizadas;
	}

	public void setTotalRealizadas(long totalRealizadas) {
		this.totalRealizadas = totalRealizadas;
	}

	@Override
	public String toString() {
		return "Prova [id=" + id + ", descricao=" + descricao + ", ativa=" + ativa + ", totalRealizadas="
				+ totalRealizadas + ", questoes=" + questoes + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Prova other = (Prova) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
